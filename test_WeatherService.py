from unittest import TestCase
from weather import weather_service


class TestWeatherService(TestCase):
    def test_get_latitude(self):
        service = weather_service.WeatherService()
        coordinates = service.get_lat_and_lon_from_city('Hamilton, ON')

        real_coords = (43.26, -79.87)
        self.assertAlmostEqual(coordinates[0], real_coords[0], places=2)
        self.assertAlmostEqual(coordinates[1], real_coords[1], places=2)

    def test_generate_box(self):
        service = weather_service.WeatherService()
        lat = 43.26
        lon = -79.87
        # try a large distance
        dist = 100
        box = service.generate_box(lat, lon, dist)
        self.assertAlmostEqual(box[0], -80.73, places=2)
        self.assertAlmostEqual(box[1], 42.62, places=2)
        self.assertAlmostEqual(box[2], -78.99, places=2)
        self.assertAlmostEqual(box[3], 43.89, places=2)

        # try a small distance
        dist = 5
        box = service.generate_box(lat, lon, dist)
        self.assertAlmostEqual(box[0], -79.91, places=2)
        self.assertAlmostEqual(box[1], 43.23, places=2)
        self.assertAlmostEqual(box[2], -79.83, places=2)
        self.assertAlmostEqual(box[3], 43.29, places=2)

        # try no distance
        dist = 0
        box = service.generate_box(lat, lon, dist)
        self.assertAlmostEqual(box[0], -79.87, places=2)
        self.assertAlmostEqual(box[1], 43.26, places=2)
        self.assertAlmostEqual(box[2], -79.87, places=2)
        self.assertAlmostEqual(box[3], 43.26, places=2)

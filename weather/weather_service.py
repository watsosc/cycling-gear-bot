import requests
import geopy

from geopy.geocoders import Nominatim
from geopy.distance import geodesic

# radius of earth in meters
earth_radius = 6378000

class WeatherService():
    def __init__(self):
        base_url = 'http://api.openweathermap.org/data/2.5'
        with open('weather/weather-api-key') as file:
            self.api_key = file.readline()
        self.weather_url = base_url + '/find'
        self.uvi_url = base_url + '/uvi'

        self.geolocator = Nominatim(user_agent='cycling_gear_bot')

    def get_lat_and_lon_from_city(self, city):
        location = self.geolocator.geocode(city)
        return (location.latitude, location.longitude)

    '''
    get weather from a list of cities based on the latitude/longitude of the starting point plus an expected distance
    '''
    def get_current_weathers(self, lat, lon, dist):
        box = self.generate_box(lat, lon, dist)
        query_params = {'appid' : self.api_key,
                        'bbox' : ','.join(map(str, box)),
                        'units' : 'metric'}
        current_weathers = requests.get(self.weather_url, params=query_params)
        if current_weathers.status_code == 200:
            return current_weathers.json()

    '''
    get ultraviolet index from a specified location
    '''
    def get_current_uvi(self, lat, lon):
        query_params = {'appid' : self.api_key,
                        'lat' : lat,
                        'lon' : lon}
        current_uvi = requests.get(self.uvi_url, params=query_params)
        if current_uvi.status_code == 200:
            return current_uvi.json()

    '''
    given a starting point and a ride distance, create a box surrounding that starting point on all sides
    by the total length of the ride
    i.e. for a 100 km ride, the square will be 200km wide
    '''
    def generate_box(self, lat, lon, dist):
        # create a square box of size dist around our origin at (lat,lon)
        origin = geopy.Point(lat, lon)
        min_corner = geodesic(kilometers=dist).destination(origin, 225)
        max_corner = geodesic(kilometers=dist).destination(origin, 45)

        # last parameter is the 'zoom', it is required by the api I'm using but... who knows why
        return [min_corner.longitude, min_corner.latitude, max_corner.longitude, max_corner.latitude, 10]
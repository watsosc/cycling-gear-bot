from ananas import PineappleBot, interval
from urllib.parse import urlparse

'''
this bot will respond when it is mentioned with an image by generating a pseudo-random compliment based on the image pixels
'''
class CyclingGearBot(PineappleBot):
    def start(self):
        self.tooting_name = 'cycling gear bot'
        self.scrape()

    '''
    pulls all mentions, gets ready to reply
    '''
    @interval(60)
    def scrape(self):
        me = self.mastodon.account_verify_credentials()
        mentions = self.get_my_mentions(me['id']);

    '''
    looks at my local timeline and filters out everything that doesn't mention me
    overall a good strategy in any part of life
    '''
    def get_my_mentions(self, id):
        toots = self.mastodon.timeline_home()
        replies = [x for x in toots if any(m['id'] == id for m in x['mentions'])]
        return replies

    '''
    get the instance name out of the user dict
    the url we get is always in the form: [http://][instance.name]/[username]
    '''
    def get_user_instance_name(self, user):
        if 'url' not in user or 'username' not in user:
            return None

        instance_address = urlparse(user['url'])
        instance_name = instance_address.netloc
        return user['username'] + '@' + instance_name
from cyclinggearbot import CyclingGearBot
from unittest import TestCase


class TestCyclingGearBot(TestCase):

    def test_get_user_instance_name(self):
        bot = CyclingGearBot('config.cfg')
        # should get the instance name out of the url field and stick the username in front with an @ between
        user = {
            'username': 'Gargron',
            'url': 'http://mastodon.social/Gargron'
        }
        instance_name = bot.get_user_instance_name(user)
        self.assertEqual(instance_name, 'Gargron@mastodon.social')

        # try one with some extra dots in it
        user = {
            'username': 'Gargron',
            'url': 'http://mastodon.even.more.social/Gargron'
        }
        instance_name = bot.get_user_instance_name(user)
        self.assertEqual(instance_name, 'Gargron@mastodon.even.more.social')

        # what if there's nothing there?
        user = {}
        instance_name = bot.get_user_instance_name(user)
        self.assertIsNone(instance_name)